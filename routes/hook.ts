import { Router, Response, Request } from 'express';
const hookcord = require('hookcord');

const Hook = new hookcord.Hook()
const hookRoute = Router(); 

hookRoute.post('/test', async(req: Request, res: Response)=>{
    
    //Creamos el cuerpo del Hook
    try {
        
        //En este apartado deberás de especificar la id del hook y el secret
        Hook.login('ID','SECRET');

        //Configuramos un payload para envío del contenido, utilizaré el formato tarjeta
        Hook.setPayload({
            "embeds": [{
              "title": "Hola, soy un Webhook.",
              "color": 15257231,
              'url': 'http://adagioti.com',
              'description': 'Una descripción',
              "fields": [
                {
                  "name": "Campo 1",
                  "value": "Otro Campo",
                  'inline': true
                }
              ],
            'timestamp': new Date()
            }]
          })

          //En este apartado lanzamos el disparador.
          Hook.fire()
                .then((resp:any)=>{
                    return res.status(200).send(resp);
                })
                .catch((err:any)=>{
                    return res.status(500).send(err)
                })

    } catch (error) {
            return res.status(500).send(error)
    }
})

export default hookRoute;