import express from 'express';
import { PORT } from '../config/config';
let env = process.env["NODE_ENV"];

export default class Server {
   
    public app: express.Application; 
    public port  = PORT; //puerto donde escuchará el api 

    constructor(){
        this.app = express(); 
    }

    start(callback: Function){
        console.log(this.port);
        this.app.listen(this.port);
    }
} 

export interface ProcessEnv {
    [key: string]: string | undefined;
}