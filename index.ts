import Server from './server/server';
import hookRoute from './routes/hook';
import bodyparser from 'body-parser';


const server = new Server(); 

server.app.use(bodyparser.urlencoded({extended: true}));
server.app.use(bodyparser.json());

server.app.use('/',hookRoute);


server.start(()=>{
    console.log('El servidor esta ejecutandose en el puerto: '+server.port);
})